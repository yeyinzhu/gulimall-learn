#### vscode 相关总结

### 1、代码片段
## 1.1 vue 
```
{
    "Print to console": {
        "prefix": "vue",
        "body": [
            "<!-- $1 -->",
            "<template>",
            "<div class='$2'>$5</div>",
            "</template>",
            "",
            "<script>",
            "//这里可以导入其他文件（比如：组件，工具js，第三方插件js，json文件，图片文件等等）",
            "//例如：import 《组件名称》 from '《组件路径》';",
            "",
            "export default {",
            "//import引入的组件需要注入到对象中才能使用",
            "components: {},",
            "data() {",
            "//这里存放数据",
            "return {",
            "",
            "};",
            "},",
            "//监听属性 类似于data概念",
            "computed: {},",
            "//监控data中的数据变化",
            "watch: {},",
            "//方法集合",
            "methods: {",
            "",
            "},",
            "//生命周期 - 创建完成（可以访问当前this实例）",
            "created() {",
            "",
            "},",
            "//生命周期 - 挂载完成（可以访问DOM元素）",
            "mounted() {",
            "",
            "},",
            "beforeCreate() {}, //生命周期 - 创建之前",
            "beforeMount() {}, //生命周期 - 挂载之前",
            "beforeUpdate() {}, //生命周期 - 更新之前",
            "updated() {}, //生命周期 - 更新之后",
            "beforeDestroy() {}, //生命周期 - 销毁之前",
            "destroyed() {}, //生命周期 - 销毁完成",
            "activated() {}, //如果页面有keep-alive缓存功能，这个函数会触发",
            "}",
            "</script>",
            "<style scoped>",
            "$4",
            "</style>"
        ],
        "description": "Log output to console"
    }
}
```

## 1.2 httpget
```
{
    "Print to console": {
        "prefix": "httpget",
        "body": [
            "this.\\$http({",
				"url: this.\\$http.adornUrl('/'),",
				"method: 'get'",
			"}).then(({ data}) => {",
				"//处理 data 数据",
			"});"
        ],
        "description": "generate http get post code"
    }
}
```

## 1.3 httppost
```
{
    "Print to console": {
        "prefix": "httppost",
        "body": [
            "let param = {};",
			"this.\\$http({",
				"url: this.\\$http.adornUrl('/'),",
				"method: 'post',",
				"data: this.\\$http.adornData(param, false)",
			"}).then(({ data",
			"}) => {",
				"if (data && data.code === 0) {",
				"this.\\$message({",
					"message: 'xxx 成功',",
					"type: 'success'",
					"});",
				"//定义成功事件",
				"} else {",
					"//显示失败信息",
					"this.\\$message.error(data.msg);",
				"}",
			"});"
        ],
        "description": "generate http get post code"
    }
}
```

## 1.4 confirmdialog 
```
{
    "Print to console": {
        "prefix": "confirmDialog",
        "body": [
			"this.\\$confirm(`确定删除[\\${}]菜单吗?`, '提示', {",
			"	confirmButtonText: '确定',",
			"	cancelButtonText: '取消',",
			"   type: 'warning'",
			  "}).then(() => {",
			"}).catch(() => {",
			"  this.\\$message({",
			"	type: 'info',",
			"	message: '已取消删除'",
			"  });",
			"});"
        ],
        "description": "generate confirm dialog"
    }
}
```