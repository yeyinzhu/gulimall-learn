#### vue 相关总结

### 1、单向、双向绑定
```
//单向绑定
<!-- 给html标签的属性绑定 -->
<div id="app"> 
    <a v-bind:href="link">gogogo</a>
    <!-- class,style  {class名：加上？}-->
    <span v-bind:class="{active:isActive,'text-danger':hasError}"
      :style="{color: color1,fontSize: size}">你好</span>
</div>
<script src="../node_modules/vue/dist/vue.js"></script>

<script>
    let vm = new Vue({
        el:"#app",
        data:{
            link: "http://www.baidu.com",
            isActive:true,
            hasError:true,
            color1:'red',
            size:'36px'
        }
    })
</script>
```

```
//双向绑定
<!-- 表单项，自定义组件 -->
<div id="app">

    精通的语言：
        <input type="checkbox" v-model="language" value="Java"> java<br/>
        <input type="checkbox" v-model="language" value="PHP"> PHP<br/>
        <input type="checkbox" v-model="language" value="Python"> Python<br/>
    选中了 {{language.join(",")}}
</div>

<script src="../node_modules/vue/dist/vue.js"></script>

<script>
    let vm = new Vue({
        el:"#app",
        data:{
            language: []
        }
    })
</script>
```

### 2、on for if
```
//on
<div id="app">
<!--事件中直接写js片段-->
<button @click="num++">点赞</button>
<!--事件指定一个回调函数，必须是Vue实例中定义的函数-->
<button @click="cancel">取消</button>
<!--  -->
<h1>有{{num}}个赞</h1>

<!-- 事件修饰符 -->
<div style="border: 1px solid red;padding: 20px;" @click.once="hello">
    大div
    <div style="border: 1px solid blue;padding: 20px;" @click.stop="hello">
        小div <br />
        <a href="http://www.baidu.com" @click.prevent.stop="hello">去百度</a>
    </div>
</div>

<!-- 按键修饰符： -->
<input type="text" v-model="num" @keyup.up="num+=2" @keyup.down="num-=2" @click.ctrl="num=10"><br />
提示：
</div>
<script src="../node_modules/vue/dist/vue.js"></script>
<script>
new Vue({
    el:"#app",
    data:{
        num: 1
    },
    methods:{
        cancel(){
            this.num--;
        },
        hello(){
            alert("点击了")
        }
    }
})
</script>
```

```
//for
<div id="app">
    <ul>
        <li v-for="(user,index) in users" :key="user.name" v-if="user.gender == '女'">
            <!-- 1、显示user信息：v-for="item in items" -->
           当前索引：{{index}} ==> {{user.name}}  ==>   {{user.gender}} ==>{{user.age}} <br>
            <!-- 2、获取数组下标：v-for="(item,index) in items" -->
            <!-- 3、遍历对象：
                    v-for="value in object"
                    v-for="(value,key) in object"
                    v-for="(value,key,index) in object" 
            -->
            对象信息：
            <span v-for="(v,k,i) in user">{{k}}=={{v}}=={{i}}；</span>
            <!-- 4、遍历的时候都加上:key来区分不同数据，提高vue渲染效率 -->
        </li>
    </ul>
    <ul>
        <li v-for="(num,index) in nums" :key="index"></li>
    </ul>
</div>
<script src="../node_modules/vue/dist/vue.js"></script>
<script>         
    let app = new Vue({
        el: "#app",
        data: {
            users: [{ name: '柳岩', gender: '女', age: 21 },
            { name: '张三', gender: '男', age: 18 },
            { name: '范冰冰', gender: '女', age: 24 },
            { name: '刘亦菲', gender: '女', age: 18 },
            { name: '古力娜扎', gender: '女', age: 25 }],
            nums: [1,2,3,4,4]
        },
    })
</script>
```

```
//if
<!-- 
    v-if，顾名思义，条件判断。当得到结果为true时，所在的元素才会被渲染。
    v-show，当得到结果为true时，所在的元素才会被显示。 
-->
<div id="app">
    <button v-on:click="show = !show">点我呀</button>
    <!-- 1、使用v-if显示 -->
    <h1 v-if="show">if=看到我....</h1>
    <!-- 2、使用v-show显示 -->
    <h1 v-show="show">show=看到我</h1>
</div>

<script src="../node_modules/vue/dist/vue.js"></script>
    
<script>
    let app = new Vue({
        el: "#app",
        data: {
            show: true
        }
    })
</script>


<div id="app">
    <button v-on:click="random=Math.random()">点我呀</button>
    <span>{{random}}</span>

    <h1 v-if="random>=0.75">
        看到我啦？！ &gt;= 0.75
    </h1>
    <h1 v-else-if="random>=0.5">
        看到我啦？！ &gt;= 0.5
    </h1>
    <h1 v-else-if="random>=0.2">
        看到我啦？！ &gt;= 0.2
    </h1>
    <h1 v-else>
        看到我啦？！ &lt; 0.2
    </h1>
</div>
<script src="../node_modules/vue/dist/vue.js"></script>  
<script>         
    let app = new Vue({
        el: "#app",
        data: { random: 1 }
    })     
</script>
```

### 3、计算属性、侦听器、过滤器
```

<div id="app">
    <!-- 某些结果是基于之前数据实时计算出来的，我们可以利用计算属性。来完成 -->
    <ul>
        <li>西游记； 价格：{{xyjPrice}}，数量：<input type="number" v-model="xyjNum"> </li>
        <li>水浒传； 价格：{{shzPrice}}，数量：<input type="number" v-model="shzNum"> </li>
        <li>总价：{{totalPrice}}</li>
        {{msg}}
    </ul>
</div>
<script src="../node_modules/vue/dist/vue.js"></script>

<script>
    //watch可以让我们监控一个值的变化。从而做出相应的反应。
    new Vue({
        el: "#app",
        data: {
            xyjPrice: 99.98,
            shzPrice: 98.00,
            xyjNum: 1,
            shzNum: 1,
            msg: ""
        },
        computed: {
            totalPrice(){
                return this.xyjPrice*this.xyjNum + this.shzPrice*this.shzNum
            }
        },
        watch: {
            xyjNum(newVal,oldVal){
                if(newVal>=3){
                    this.msg = "库存超出限制";
                    this.xyjNum = 3
                }else{
                    this.msg = "";
                }
            }
        },
    })
</script>

//过滤器

<!-- 过滤器常用来处理文本格式化的操作。过滤器可以用在两个地方：双花括号插值和 v-bind 表达式 -->
<div id="app">
    <ul>
        <li v-for="user in userList">
            {{user.id}} ==> {{user.name}} ==> {{user.gender == 1?"男":"女"}} ==>
            {{user.gender | genderFilter}} ==> {{user.gender | gFilter}}
        </li>
    </ul>
</div>
<script src="../node_modules/vue/dist/vue.js"></script>

<script>
    Vue.filter("gFilter", function (val) {
        if (val == 1) {
            return "男~~~";
        } else {
            return "女~~~";
        }
    })

    let vm = new Vue({
        el: "#app",
        data: {
            userList: [
                { id: 1, name: 'jacky', gender: 1 },
                { id: 2, name: 'peter', gender: 0 }
            ]
        },
        filters: {
            //// filters 定义局部过滤器，只可以在当前vue实例中使用
            genderFilter(val) {
                if (val == 1) {
                    return "男";
                } else {
                    return "女";
                }
            }
        }
    })
</script>
```

### 4、组件化
```

<div id="app">
<button v-on:click="count++">我被点击了 {{count}} 次</button>

<counter></counter>
<counter></counter>
<counter></counter>
<counter></counter>
<counter></counter>

<button-counter></button-counter>
</div>
<script src="../node_modules/vue/dist/vue.js"></script>

<script>
//1、全局声明注册一个组件
Vue.component("counter", {
    template: `<button v-on:click="count++">我被点击了 {{count}} 次</button>`,
    data() {
        return {
            count: 1
        }
    }
});

//2、局部声明一个组件
const buttonCounter = {
    template: `<button v-on:click="count++">我被点击了 {{count}} 次~~~</button>`,
    data() {
        return {
            count: 1
        }
    }
};
new Vue({
    el: "#app",
    data: {
        count: 1
    },
    components: {
        'button-counter': buttonCounter
    }
})
</script>
```

### 5、生命周期和钩子函数
```
<div id="app">
    <span id="num">{{num}}</span>
    <button @click="num++">赞！</button>
    <h2>{{name}}，有{{num}}个人点赞</h2>
</div>
<script src="../node_modules/vue/dist/vue.js"></script>
<script>
    let app = new Vue({
        el: "#app",
        data: {
            name: "张三",
            num: 100
        },
        methods: {
            show() {
                return this.name;
            },
            add() {
                this.num++;
            }
        },
        beforeCreate() {
            console.log("=========beforeCreate=============");
            console.log("数据模型未加载：" + this.name, this.num);
            console.log("方法未加载：" + this.show());
            console.log("html模板未加载：" + document.getElementById("num"));
        },
        created: function () {
            console.log("=========created=============");
            console.log("数据模型已加载：" + this.name, this.num);
            console.log("方法已加载：" + this.show());
            console.log("html模板已加载：" + document.getElementById("num"));
            console.log("html模板未渲染：" + document.getElementById("num").innerText);
        },
        beforeMount() {
            console.log("=========beforeMount=============");
            console.log("html模板未渲染：" + document.getElementById("num").innerText);
        },
        mounted() {
            console.log("=========mounted=============");
            console.log("html模板已渲染：" + document.getElementById("num").innerText);
        },
        beforeUpdate() {
            console.log("=========beforeUpdate=============");
            console.log("数据模型已更新：" + this.num);
            console.log("html模板未更新：" + document.getElementById("num").innerText);
        },
        updated() {
            console.log("=========updated=============");
            console.log("数据模型已更新：" + this.num);
            console.log("html模板已更新：" + document.getElementById("num").innerText);
        }
    });
</script>
```

### 6、模块化开发
学习地址:
https://router.vuejs.org/zh/guide/

## 6.1 全局安装 webpack
```
npm install weppack -g
```

## 6.2 全局安装vue脚手架
```
npm install -g @vue/cli-init
```

## 6.3 初始化 vue项目;
```
vue init webpack appname: vue脚手架使用 webpack 模板初始化-个appname 项目
```

初始化项目依次选择下面的选项：
![vue-module-develop-1](https://gitee.com/yeyinzhu/gulimall-learn/raw/master/images/vue/module-develop/1.png)

## 6.4 启动 vue 项目;
项目的package.json中有scripts, 代表我们能运行的命令
npm start = npm run dev:启动项目
npm run build:将项目打包

遇到问题
1、vue not found,执行如下命令即可
npm install -g vue-cli

简单开发总结：
1、书写 .vue 文件,在 src/compents 目录下.
```
//vue 包含 3 部分：template、script、style
<template>
  <div>
    <h3>Hello, {{userName}}</h3>
  </div>
</template>
<script>
    export default {
        data() {
            return {
            userName: "bamboo"
            };
        }
    };
</script>
<style>
</style>
```

2、定制 router，在 router/index.js 中配置
```
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Hello from '@/components/Hello'//@ 表示根目录 

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },{
      path: '/hello',
      name: 'hello',
      component: Hello
    }
  ]
})
```

3、添加链接，使用 router-link 标签
```
<router-link to="/hello">go to hello</router-link>
<router-link to="/">go to index</router-link>
```

### 7、整合 element-ui
## 7.1 安装 element-ui
```
npm i element-ui -S
```

安装好之后可以在 package.json 里看到安装的信息
```
"dependencies": {
    "element-ui": "^2.13.0",
    "vue": "^2.5.2",
    "vue-router": "^3.0.1"
}
```


## 7.2 使用，在 main.js 中书写如下的内容：
```
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';

Vue.use(ElementUI);
```

## 7.3 导航点击事件总结
1)、提取 MyTable 组件
```
<template>
  <div class>
    <el-table :data="tableData">
      <el-table-column prop="date" label="日期" width="140"></el-table-column>
      <el-table-column prop="name" label="姓名" width="120"></el-table-column>
      <el-table-column prop="address" label="地址"></el-table-column>
    </el-table>
  </div>
</template>
<script>
//这里可以导入其他文件（比如：组件，工具js，第三方插件js，json文件，图片文件等等）
//例如：import 《组件名称》 from '《组件路径》';
export default {
  //import引入的组件需要注入到对象中才能使用
  components: {},
  data() {
    //这里存放数据
    const item = {
      date: "2016-05-02",
      name: "王小虎",
      address: "上海市普陀区金沙江路 1518 弄"
    };
    return {
      tableData: Array(20).fill(item)
    };
  }
};
</script>
<style scoped>
</style>
```

2)、写 router 规则:
```
import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Hello from '@/components/Hello'
import MyTable from '@/components/MyTable'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'HelloWorld',
      component: HelloWorld
    },{
      path: '/hello',
      name: 'hello',
      component: Hello
    },{
      path: '/myTable',
      name: 'myTable',
      component: MyTable
    }
  ]
})
```


3)、修改 menu 的点击事件(添加 :router="true"、index 写 path 路径)，router 前加 : 是为了保证不报错.(如果不加报 如下的错) Invalid prop: type check failed for prop "router". Expected Boolean, got String with value "true".
//这里添加 :router="true" 属性
```
<el-menu :default-openeds="['1', '3']" :router="true">
<el-submenu index="1">
  <template slot="title">
    <i class="el-icon-message"></i>导航一
  </template>
  <el-menu-item-group>
    <template slot="title">分组一</template>
    //index 里填写 router 的 path.
    <el-menu-item index="/myTable">用户列表</el-menu-item>
    <el-menu-item index="/hello">hello</el-menu-item>
```

### 8、父子组件
## 8.1 common 包下封装 category 组件
common 目录位于 modules 目录下，跟 product 在同一级
```
<template>
  <el-tree :data="menus" :props="defaultProps" node-key="catId" ref="menuTree" @node-click="nodeClick"></el-tree>
</template>

<script>
export default {
  data() {
    //这里存放数据
    return {
      menus: [],
      defaultProps: {
        children: "children",
        label: "name"
      }
    };
  },
  //方法集合
  methods: {
    getMenus() {
      this.$http({
        url: this.$http.adornUrl("/product/category/list/tree"),
        method: "get"
      }).then(({ data }) => {
        this.menus = data.data;
      });
    }, 
    nodeClick (data, node, component) {
        //父子组件传递事件 这里的 tree-node-click 跟父组件中的一致
        this.$emit('tree-node-click', data, node, component);
    }
  },
  //生命周期 - 创建完成（可以访问当前this实例）
  created() {
    this.getMenus();
  }
};
</script>
<style scoped>
</style>
```


## 8.2 引入公共组件 
```
import Category from "../common/category";
import AddOrUpdate from "./attrgroup-add-or-update";
export default {
  //import引入的组件需要注入到对象中才能使用
  components: { Category, AddOrUpdate }
}
```

## 8.3 定义接收子组件的事件
```
//刚刚定义了 Category 组件,现在直接使用即可.
//子组件中 this.$emit('tree-node-click', data, node, componment) 定义了事件,所以这里直接引用 
//@后面的 tree-node-click 和 $emit 中的第一个参数保持一致
<Category @tree-node-click="treeNodeClick"></Category>
```

## 8.4 然后定义事件即可
```
methods : {
    treeNodeClick (data, node, componment) {
        console.log(data, node, componment)
    }
}
```


### 9、级联菜单
## 9.1 前端代码
```
//:options="categorys" 这个表示显示的选项, categorys 是 vue 组件中 data 中的字段.
//v-model="dataFrom.catlogIds" 绑定的选择字段,这里是一个数组,最终要提交的是 1 个值.所以声明 2 个字段
//:props="categoryCascaders" 表示显示的信息绑定  label 表示显示值,value 表示真正的值
<el-form-item label="所属分类id" prop="catelogId">
    <el-cascader v-model="dataForm.catelogIds" :options="categorys" :props="categoryCascaders"></el-cascader>
</el-form-item>

// data 中绑定的值
data() {
    return {
      categorys: [],
      visible: false,
      dataForm: {
        attrGroupId: 0,
        attrGroupName: "",
        sort: "",
        descript: "",
        icon: "",
        catelogId: "",
        catelogIds: []
      },
      categoryCascaders: {
        label: "name",
        value: "catId",
        children: "children"
      },
    }
},
 methods: {
    getMenus() {
      this.$http({
        url: this.$http.adornUrl("/product/category/list/tree"),
        method: "get"
      }).then(({ data }) => {
          //这里给 categorys 赋值
        this.categorys = data.data;
      });
    }
  },
  created() {
      //组件创建完成的时候调用 getMenus()
    this.getMenus();
  }
```



## 9.2 级联菜单的回显
# 9. 2.1 后端代码开发
```
//获取完整的菜单 id 
@Override
public List<Long> findCatelogPath(Long catelogId) {
    List<Long> resultList = Lists.newArrayList();
    // Long[] result = paths.toArray(new Long[paths.size()]);
    findParentPath(catelogId, resultList);
    Collections.reverse(resultList);
    return resultList;
}

private void findParentPath(Long catelogId, List<Long> paths) {
    paths.add(catelogId);
    CategoryEntity categoryEntity = getById(catelogId);
    if (Objects.nonNull(categoryEntity)) {
        Long parentCid = categoryEntity.getParentCid();
        if (Objects.nonNull(parentCid) && parentCid != 0L) {
            findParentPath(parentCid, paths);
        }
    }
}

//设置属性
@RequestMapping("/info/{attrGroupId}")
public R info(@PathVariable("attrGroupId") Long attrGroupId) {
    AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);

    if (Objects.nonNull(attrGroup)) {
        Long catelogId = attrGroup.getCatelogId();
        List<Long> catelogIds = categoryService.findCatelogPath(catelogId);
        //返回值添加 pathId 的完整路径 
        attrGroup.setCatelogIds(catelogIds);
    }

    return R.ok().put("attrGroup", attrGroup);
}

//实体属性添加 json 信息,这个注解标识不是数据库的字段
@TableField(exist = false)
private List<Long> catelogIds;
```

# 9.2.2 前端代码 
```
//添加赋值操作,在点击修改获取数据成功的函数中添加 
this.dataForm.catelogIds = data.attrGroup.catelogIds;

//菜单修改的时候 catelogId 取的是最后的那个值
catelogId: this.dataForm.catelogIds[this.dataForm.catelogIds.length - 1]

//菜单关闭的时候重置 catelogIds 属性,保证我新增按钮的时候不把上一次的菜单带出来 
dialogClose() {
  this.dataForm.catelogIds = [];
}

//el-dialog 添加属性 @closed 
<el-dialog
    :title="!dataForm.id ? '新增' : '修改'"
    :close-on-click-modal="false"
    :visible.sync="visible"
    @closed="dialogClose"
  >
```


